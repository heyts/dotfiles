# .bash_profile (.bashrc on linux)
# Some aapted from https://github.com/paulirish/dotfiles

# PATH
PATH=/opt/local/bin
PATH=$PATH:/opt/local/sbin
PATH=$PATH:/bin
PATH=$PATH:/sbin
PATH=$PATH:/usr/bin
PATH=$PATH:/usr/sbin
PATH=/usr/local/bin:$PATH
PATH=$PATH:~/code/bin
PATH=$PATH:/usr/local/go/bin
export PATH

# Prefer US English and use UTF-8
export LC_ALL="en_US.UTF-8"
export LANG="en_US"

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2)" scp sftp ssh

alias hosts='sudo $EDITOR /etc/hosts'   # yes I occasionally 127.0.0.1 twitter.com ;)

# Pretty colors in tmux
alias tmux="TERM=screen-256color-bce tmux"

# Modify vim, tmux or bash
alias vimrc="$EDITOR ~/.vim/vimrc"
alias bashrc="$EDITOR ~/.bash_profile"
alias irssi='TERM=screen-256color irssi'
PS1='\[\e[1;32m\][\u@\h \W]\$\[\e[0m\] '

# alias sl = ls
alias sl="ls"

# List all files colorized in long format
alias l="ls -l ${colorflag}"

# List all files colorized in long format, including dot files
alias la="ls -la ${colorflag}"

# `cat` with beautiful colors. requires Pygments installed.
alias c='pygmentize -O style=monokai -f console256 -g'

# Use python 3 by default

alias python='python3'

# Recursively delete `.DS_Store` files
alias cleanup="find . -name '*.DS_Store' -type f -ls -delete"

# Shortcuts
alias g="git"
alias v="vim"
alias gitpretty="git log --abbrev-commit --pretty=oneline"

# Empty the Trash on all mounted volumes and the main HDD
alias emptytrash="sudo rm -rfv /Volumes/*/.Trashes; rm -rfv ~/.Trash"

# Make vim the default editor
export EDITOR="vim"

# Don’t clear the screen after quitting a manual page
export MANPAGER="less -X"

# Larger bash history (allow 32³ entries; default is 500)
export HISTSIZE=32768
export HISTFILESIZE=$HISTSIZE
export HISTCONTROL=ignoredups

# timestamps for bash history. www.debian-administration.org/users/rossen/weblog/1
# saved for later analysis
HISTTIMEFORMAT='%F %T '
export HISTTIMEFORMAT

# Make some commands not show up in history
export HISTIGNORE="ls:ls *:cd:cd -:pwd;exit:date:* --help"
export TERM=xterm-256color
# Start postgresql
alias startpsql="launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist"
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

# Docker path
export DOCKER_TLS_VERIFY=1
export DOCKER_HOST=tcp://192.168.59.103:2376
export DOCKER_CERT_PATH=/Users/heyts/.boot2docker/certs/boot2docker-vm

export PATH=/Applications/Postgres.app/Contents/Versions/9.4/bin:$PATH

export NVM_DIR="/Users/heyts/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
