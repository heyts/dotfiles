dotfiles
========

These are the dotfiles I use. I started using vim around a year ago and started by using the dotfiles of various people (notably [Paul Irish][1] and [ThoughtBot][2]).

After a while I realized that the amount of settings contained in those files were counterproductive to my understanding of bash and vim (mostly). I decided to start fresh and only use very minimal .vimrc and .bashrc files, slowly building up to the configuration I needed. 

Feel free to use these files but beware that they may not suit you need (I mostly do python & javascript).

[1]:https://github.com/paulirish/dotfiles
[2]:https://github.com/thoughtbot/dotfiles
